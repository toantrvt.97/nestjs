import { Injectable } from '@nestjs/common';
import { Employee } from './employee.entity'
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UpdateResult, DeleteResult } from 'typeorm';
import { from } from 'rxjs';
@Injectable()

export class EmployeeService {
    constructor(
        @InjectRepository(Employee)
        private readonly employRepo: Repository<Employee>
    ) { }
    async findAll(): Promise<Employee[]> {
        return await this.employRepo.find();
    }
    async findOne(id: number): Promise<Employee> {
        return await this.employRepo.findOne(id);
    }
    async create(employee: Employee): Promise<Employee> {
        return await this.employRepo.save(employee)
    }
    async update(employee: Employee): Promise<UpdateResult> {
        return await this.employRepo.update(employee.id, employee);
    }
    async delete(id): Promise<DeleteResult> {
        return await this.employRepo.delete(id);
    }
}
