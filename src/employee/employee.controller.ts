import { Controller, Get, Post, Put, Delete, Body, Param } from '@nestjs/common';
import { EmployeeService } from './employee.service'
import { Employee } from './employee.entity'

@Controller('employee')
export class EmployeeController {
    constructor(private readonly employeeService: EmployeeService) { }
    @Get()
    findAll(): Promise<Employee[]> {
        return this.employeeService.findAll();
    }
    @Get(':id')
    get(@Param() params) {
        return this.employeeService.findOne(params.id);
    }
    @Post()
    create(@Body() employee: Employee) {
        return this.employeeService.create(employee);
    }
    @Put()
    update(@Body() employee: Employee) {
        return this.employeeService.update(employee);
    }
    @Delete('id:')
    delete(@Param() params) {
        return this.employeeService.delete(params.id);
    }

}

