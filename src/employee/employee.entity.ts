import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
export class Employee {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 50 })
    username: string;

    @Column({ length: 8 })
    password: string;

    @Column()
    email: string;

    @Column()
    employeename: string;

    @Column()
    address: string;

    @Column()
    phone: string;

    @Column()
    ismanager: boolean;

    @Column()
    role: string;
}